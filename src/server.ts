import * as express from 'express';
import * as cors from 'cors';
import { TransactionEndpoint } from './transaction/transaction.endpoint';
export const app: express.Application = express();

app.use(cors());
const transactionEndpoint = new TransactionEndpoint(app);
