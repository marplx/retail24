import { Request, Response, NextFunction } from 'express';
import * as HttpStatus from 'http-status-codes';
import { extractConfidenceFromQueryParam, extractTransactionIdFromQueryParam } from './query-param.extractor';

describe('Query Param Extractor', () => {
  let req: any;
  let res: any;
  let nextMock: jest.Mock;

  beforeEach(() => {
    req = {
      query: {}
    };
    res = {
      locals: Object,
      status: jest.fn(),
      send: jest.fn()
    };
    res.status.mockReturnValue(res);
    nextMock = jest.fn();
  });

  describe('Transaction ID extractor', () => {
    it('should save a transaction ID in the response locals and call next()', () => {
      req.query.transactionId = 'foo-id';

      extractTransactionIdFromQueryParam(req, res, nextMock);

      expect(res.locals.transactionId).toEqual('foo-id');
      expect(nextMock).toHaveBeenCalled();
    });

    it('should not call next() with missing transactionId', () => {
      req.query.transactionId = undefined;

      extractTransactionIdFromQueryParam(req, res, nextMock);

      expect(nextMock).not.toHaveBeenCalled();
      expect(res.status).toHaveBeenCalledWith(HttpStatus.BAD_REQUEST);
      expect(res.send).toHaveBeenCalled();
    });
  });

  describe('Confidence Level extractor', () => {
    it('should save a parsed confidence level in the response locals and call next()', () => {
      req.query.confidenceLevel = '0.123';

      extractConfidenceFromQueryParam(req, res, nextMock);

      expect(res.locals.confidenceLevel).toEqual(0.123);
      expect(nextMock).toHaveBeenCalled();
    });

    it('should not call next() with missing confidenceLevel', () => {
      req.query.confidenceLevel = undefined;

      extractConfidenceFromQueryParam(req, res, nextMock);

      expect(nextMock).not.toHaveBeenCalled();
      expect(res.status).toHaveBeenCalledWith(HttpStatus.BAD_REQUEST);
      expect(res.send).toHaveBeenCalled();
    });

    it('should not call next() with an unparsable confidenceLevel', () => {
      req.query.confidenceLevel = 'foo';

      extractConfidenceFromQueryParam(req, res, nextMock);

      expect(nextMock).not.toHaveBeenCalled();
      expect(res.status).toHaveBeenCalledWith(HttpStatus.BAD_REQUEST);
      expect(res.send).toHaveBeenCalled();
    });
  });
});
