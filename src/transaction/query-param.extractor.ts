import { Request, Response, NextFunction } from 'express';
import * as HttpStatus from 'http-status-codes';

export function extractTransactionIdFromQueryParam(req: Request, res: Response, next: NextFunction) {
  const transactionId: string = req.query.transactionId;
  if (transactionId) {
    res.locals.transactionId = transactionId;
    next();
  } else {
    return res.status(HttpStatus.BAD_REQUEST).send(`Please specify a transaction ID.`);
  }
}

export function extractConfidenceFromQueryParam(req: Request, res: Response, next: NextFunction) {
  const confidenceLevel: number = Number.parseFloat(req.query.confidenceLevel);
  if (Number.isNaN(confidenceLevel)) {
    return res.status(HttpStatus.BAD_REQUEST).send(`"${confidenceLevel}" is not a valid confidence.`);
  } else {
    res.locals.confidenceLevel = confidenceLevel;
    next();
  }
}


/**
 * Adds minConfidence and transactionId to Express' response locals
 */
export interface ResponseWithTypedLocals extends Response {
  locals: {
    minConfidence: number,
    transactionId: string,
    [key: string]: any
  }

}
