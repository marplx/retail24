import * as transactions from '../test-data.json';
import { ChildTransaction, TopLevelTransaction, BaseTransaction } from './transaction.model';

/**
 * A mocked database that returns a static demo json.
 */
export class TransactionDatabase {

  public loadAllTransactions(): BaseTransaction[] {
    return transactions;
  }

  public loadTransactionById(id: string): TopLevelTransaction | ChildTransaction {
    const allTransactions: BaseTransaction[] = this.loadAllTransactions();
    return this.findById(allTransactions, id);
  }

  /**
   * Find a transaction by ID in a given nested list of transactions.
   * This method calls itself for recursion.
   * See requirement 1.b)
   * @param transactionList a nested list of transactions
   * @param id
   */
  private findById(transactionList: (TopLevelTransaction | ChildTransaction)[], id: string): BaseTransaction | ChildTransaction {

    for (const current of transactionList) {
      if (current.id === id) {
        return current;
      } else if (current.children && current.children.length > 0) {
        const childrenMatch: BaseTransaction | ChildTransaction = this.findById(current.children, id);
        if (childrenMatch) {
          return childrenMatch;
        }
      }
    }
  }

}
