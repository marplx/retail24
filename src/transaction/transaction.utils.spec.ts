import { flattenTransaction } from './transaction.utils';
import { ChildTransaction } from './transaction.model';
import { ChildTransactionBuilder } from '../testdoubles/child-transaction.builder';

describe('Transaction Utils', () => {

  it('should remove connection info from root transaction', () => {
    const childTransactionWithoutChildren: ChildTransaction = ChildTransactionBuilder.new.withId('1').build();
    const result = flattenTransaction(childTransactionWithoutChildren);
    expect(result.length).toEqual(1);
    expect(result[0]).not.toHaveProperty('connectionInfo');
  });

  it('should remove children property from root transaction', () => {
    const childTransactionWithoutChildren: ChildTransaction = ChildTransactionBuilder.new.withId('1').build();
    const result = flattenTransaction(childTransactionWithoutChildren);
    expect(result.length).toEqual(1);
    expect(result[0]).not.toHaveProperty('children');
  });

  describe('flattenList', () => {
    const transactionWithChildren: ChildTransaction = ChildTransactionBuilder.new
      .withId('1')
      .withConfidence(0.5)
      .withConnectionType('a')
      .withChildren([
        ChildTransactionBuilder.new
          .withId('1.1')
          .withConfidence(0.69)
          .withConnectionType('b')
          .build(),
        ChildTransactionBuilder.new
          .withId('1.2')
          .withConfidence(0.8)
          .withConnectionType('c')
          .withChildren([
            ChildTransactionBuilder.new
              .withId('1.2.1')
              .withConfidence(0.5)
              .withConnectionType('d')
              .build(),
            ChildTransactionBuilder.new
              .withId('1.2.2')
              .withConfidence(0.1)
              .withConnectionType('e')
              .build()
          ])
          .build()
      ]).build();

    it('should assemble a transaction with children in the correct order', () => {
      const result = flattenTransaction(transactionWithChildren);

      expect(result.length).toEqual(5);
      // first element is root transaction
      expect(result[0].id).toEqual('1');
      // then the children
      expect(result[1].id).toEqual('1.1');
      expect(result[2].id).toEqual('1.2');
      expect(result[3].id).toEqual('1.2.1');
      expect(result[4].id).toEqual('1.2.2');
    });

    it('should multiply each confidence with the parent confidence and assume root confidence = 1', () => {
      const result = flattenTransaction(transactionWithChildren);

      // 1.1
      expect(result[1].combinedConnectionInfo.confidence).toEqual(1 * 0.69);
      // 1.2
      expect(result[2].combinedConnectionInfo.confidence).toEqual(1 * 0.8);
      // 1.2.1
      expect(result[3].combinedConnectionInfo.confidence).toEqual(0.8 * 0.5);
      // 1.2.2
      expect(result[4].combinedConnectionInfo.confidence).toEqual(0.8 * 0.1);
    });

    it('should concat each connection type with all parent connection type, ignoring the root connection type', () => {
      const result = flattenTransaction(transactionWithChildren);

      // 1.1
      expect(result[1].combinedConnectionInfo.types).toEqual(['b']);
      // 1.2
      expect(result[2].combinedConnectionInfo.types).toEqual(['c']);
      // 1.2.1
      expect(result[3].combinedConnectionInfo.types).toEqual(['c', 'd']);
      // 1.2.2
      expect(result[4].combinedConnectionInfo.types).toEqual(['c', 'e']);
    });
  });

  describe('should handle corrupt data', () => {
    it('should handle a missing child property as no children', () => {
      const corruptTransaction: ChildTransaction = ChildTransactionBuilder.new
        .withId('1')
        .withChildren(undefined)
        .build();

      const result = flattenTransaction(corruptTransaction);
      expect(result.length).toEqual(1); // only root
    });

    it('should handle a missing nested child property as no children', () => {
      const corruptTransaction: ChildTransaction = ChildTransactionBuilder.new
        .withId('1')
        .withChildren([
          ChildTransactionBuilder.new
            .withId('1.2')
            .withChildren(undefined)
            .build()
        ])
        .build();

      const result = flattenTransaction(corruptTransaction);
      expect(result.length).toEqual(2);
    });

    it('should handle a missing connection info', () => {
      const corruptTransaction: ChildTransaction = ChildTransactionBuilder.new
        .withId('1')
        .withoutConnectionInfo()
        .build();

      const result = flattenTransaction(corruptTransaction);
      expect(result.length).toEqual(1);
    });

    it('should handle a missing nested connection info', () => {
      const corruptTransaction: ChildTransaction = ChildTransactionBuilder.new
        .withId('1')
        .withChildren([
          ChildTransactionBuilder.new
            .withId('1.2')
            .withoutConnectionInfo()
            .build()
        ])
        .build();

      const result = flattenTransaction(corruptTransaction);
      expect(result.length).toEqual(2);
    });
  });

});
