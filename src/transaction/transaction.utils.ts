import { ChildTransaction, FlatTransaction, TopLevelTransaction, BaseTransaction } from './transaction.model';

export function flattenTransaction(transaction: (TopLevelTransaction | ChildTransaction)): [BaseTransaction, ...FlatTransaction[]] {
  const flatChildren: FlatTransaction[] = [];
  if (transaction.children) {
    // Root transaction's confidence will always be 1 (see requirement 1.d.1)
    // Root transaction's connectionType are not taken into account (see requirement 1.d.2)
    flattenList(transaction.children, 1, [], flatChildren);
  }

  return [
    // 1.c) Strip out children and connection info for first element
    {
      id: transaction.id,
      age: transaction.age,
      name: transaction.name,
      email: transaction.email,
      phone: transaction.phone,
      geoInfo: transaction.geoInfo
    },
    ...flatChildren
  ];
}

/**
 * Flattens a given list of nested transactions.
 * Creates a combinedConnectionInfo, taking the parent's confidence and connection types into account.
 * This method is called recursively for all children lists.
 * @param transactionList a list of transactions with (optional) child transactions
 * @param parentConfidence the confidence of the lists parent transaction
 * @param parentConnectionTypes a list of connection types of all parents until the root transaction
 * @param result an array of FlatTransactions to fill
 */
function flattenList(transactionList: ChildTransaction[], parentConfidence: number, parentConnectionTypes: string[], result: FlatTransaction[]) {
  for (const transaction of transactionList) {
    // ⚠️ Fallbacks for some corrupt transactions that have no connection info (eg. 5c868b9b6c4fc9fe97f61d7b)
    const connectionTypes: string[] = transaction.connectionInfo ? [...parentConnectionTypes, transaction.connectionInfo.type] : [...parentConnectionTypes];
    const confidence: number = (transaction.connectionInfo || {}).confidence || 0;

    result.push(<FlatTransaction> {
      id: transaction.id,
      age: transaction.age,
      name: transaction.name,
      email: transaction.email,
      phone: transaction.phone,
      geoInfo: transaction.geoInfo,
      connectionInfo: transaction.connectionInfo,
      combinedConnectionInfo: {
        // 1.d.1) each child combinedConnectionInfo confidence is calculated based on the multiplication of its own
        // connectionInfo confidence with its parent’s connectionInfo confidence
        confidence: confidence * parentConfidence,
        // 1.d.2) The combinedConnectionInfo types should be a list of strings containing the connectionInfo type present in the
        // transaction, along with all of the connectionInfo types present in the parents, until getting to the main
        // transaction of transactionId in the query param.
        types: connectionTypes
      }
    });

    // ⚠️ Null-check for some corrupt transactions that have no children property (eg. 5c868b22d84354bef2474acb)
    if (transaction.children && transaction.children.length) {
      flattenList(transaction.children, transaction.connectionInfo.confidence, connectionTypes, result);
    }

  }
}
